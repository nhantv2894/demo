package thk.example.fcm.demo.dto;

import java.io.Serializable;

public class FcmMessage implements Serializable {
    private String to;
    private FcmNotification notification;

    public FcmMessage(String to, FcmNotification notification) {
        this.to = to;
        this.notification = notification;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public FcmNotification getNotification() {
        return notification;
    }

    public void setNotification(FcmNotification notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        return "FcmMessage{" +
                "to='" + to + '\'' +
                ", notification=" + notification +
                '}';
    }
}
