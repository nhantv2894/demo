package thk.example.fcm.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import thk.example.fcm.demo.dto.PnsRequest;
import thk.example.fcm.demo.service.FCMService;

import java.util.concurrent.ExecutionException;

@RestController
public class PushNotificationController {

    @Autowired
    private FCMService fcmService;

    @PostMapping("/notification")
    @ResponseBody
    public Object sendSampleNotification(@RequestBody PnsRequest pnsRequest) {
        try {
            return fcmService.pushNotification(pnsRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/check")
    public String checkInitial(@RequestBody PnsRequest pnsRequest){
        return fcmService.checkInitial(pnsRequest);
    }
}
