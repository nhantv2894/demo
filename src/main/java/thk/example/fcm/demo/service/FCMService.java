package thk.example.fcm.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import thk.example.fcm.demo.common.JSONUtils;
import thk.example.fcm.demo.dto.FcmMessage;
import thk.example.fcm.demo.dto.FcmNotification;
import thk.example.fcm.demo.dto.PnsRequest;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.URI;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class FCMService {
    @Value("${google.fcm.apis.url}")
    private String FCM_API_URL;
    @Value("${google.fcm.apis.server.key}")
    private String FCM_API_SERVER_KEY;

    private static String FCM_MESSAGE_TITLE = "Message from Java Server";

    private static final HttpClient client = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

    public String pushNotification(PnsRequest pnsRequest) throws JsonProcessingException, ExecutionException, InterruptedException {
        FcmMessage fcmMessage = new FcmMessage(pnsRequest.getFcmToken(), new FcmNotification(FCM_MESSAGE_TITLE, pnsRequest.getContent()));

        JsonObject inputJson = new JsonObject();
        inputJson.addProperty("to", pnsRequest.getFcmToken());
        JsonObject notification = new JsonObject();
        notification.addProperty("title", FCM_MESSAGE_TITLE);
        notification.addProperty("body", pnsRequest.getContent());
        inputJson.add("notification", notification);

        HttpRequest request = HttpRequest.newBuilder(URI.create(FCM_API_URL))
                .header("Content-Type", "application/json")
                .header("Authorization", FCM_API_SERVER_KEY)
//                .POST(HttpRequest.BodyPublishers.ofString(JSONUtils.covertFromObjectToJson(fcmMessage))).build();
                .POST(HttpRequest.BodyPublishers.ofString(inputJson.toString())).build();
        CompletableFuture<HttpResponse<String>> response = client.sendAsync(request,HttpResponse.BodyHandlers.ofString());
        return response.get().body();
    }

    public String checkInitial(PnsRequest pnsRequest) {
        Message message = Message.builder()
                .putData("content", pnsRequest.getContent())
                .setToken(pnsRequest.getFcmToken())
                .setNotification(Notification.builder().setTitle(FCM_MESSAGE_TITLE).setBody(pnsRequest.getContent()).build())
                .build();

        String response = null;
        try {
            response = FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException e) {
            e.printStackTrace();
        }
        return response;
    }
}
